package Ejercicio2_Persona;

public class app {
    public static void main(String[] args) {

        Persona YoXD = Persona.builder().build();
        YoXD.Inicializar();
        System.out.println(YoXD.toString());

        Persona otraPersona = new Persona("Otra",30);
        System.out.println(otraPersona.toString());

        if (YoXD.EsMayor()) {
            System.out.println(YoXD.getNombre() + " es mayor de edad");
        }
        else
            System.out.println(YoXD.getNombre() + " no es mayor de edad");
    }
}

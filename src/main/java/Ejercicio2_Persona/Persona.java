package Ejercicio2_Persona;
import lombok.*;
import java.util.Scanner;

@Builder
@Data
public class Persona {
    private String Nombre;
    private int Edad;

    public void Inicializar(){
       Scanner Teclado = new Scanner(System.in);
        System.out.print("Ingrese nombre: ");
        Nombre = Teclado.nextLine();
        System.out.print("Edad: ");
        Edad = Teclado.nextInt();
    }

    public boolean EsMayor(){
        if (Edad>=18)
            return true;
        else
            return false;
    }

}

package Ejercicio1_Hipotenusa;

import java.util.Scanner;

public class app {
    public static void main(String[] args) {

        Scanner leer = new Scanner(System.in);

        Hipotenusa h1 = Hipotenusa.builder()
                                    .factor(2)
                                    .lado1(3)
                                    .lado2(4).build();

        Hipotenusa h2 = Hipotenusa.builder().factor(2).build();

        System.out.print("Ingresa el lado 1: ");
        h2.setLado1(leer.nextInt());
        System.out.print("Ingresa el lado 2: ");
        h2.setLado2(leer.nextInt());

        System.out.println("La Hipotenusa 1 es: "+ h1.CalHipo());
        System.out.println("La Hipotenusa 2 es: "+ h2.CalHipo());

    }
}

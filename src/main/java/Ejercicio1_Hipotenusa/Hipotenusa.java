package Ejercicio1_Hipotenusa;

import lombok.*;

@Builder
@Data
public class Hipotenusa {

    private double lado1;
    private double lado2;
    private double factor;

    public double CalHipo (){
        return Math.sqrt(Math.pow(this.getLado1(),this.getFactor())+ Math.pow(this.getLado2(),this.getFactor()));
    }

}

package Ejercicio3_AreaVolumen;

import lombok.Data;

@Data
public abstract class BasicData {
    float a = 6;
    float b = 5;
    float c = 2;
}

package Ejercicio3_AreaVolumen;

public class app {
    public static void main(String[] args) {
        AreaRect x = AreaRect.builder().build();
        VolumeBox y = VolumeBox.builder().build();

        x.CalcArea();
        y.CalcVolume();
    }
}

package Ejercicio3_AreaVolumen;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class VolumeBox extends BasicData {
    float v;

    public void CalcVolume(){
        v = a * b * c;
        System.out.println("Volume is " + v);
    }
}
